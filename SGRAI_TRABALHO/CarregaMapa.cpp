#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "CarregaMapa.h"
#include <vector>
using namespace std;
// Date constructor
Joss::Joss()
{
}


vector<vector<vector<int>>> Joss::carregaDeFicheiro() {
	string line;
	string line1;
	string char1;
	ifstream myfile;

	vector<vector<vector<int>>> mapa;
	myfile.open("level1.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			vector<vector<int>> matrix;
			stringstream ss1(line);
			while (getline(ss1, line1, '.'))
			{
				
				line1.erase(0, 1);
				line1.erase(line1.end() - 1, line1.end());
				stringstream ss(line1);
				vector<int> linha;
				while (getline(ss, char1, ','))
				{
					linha.push_back(atoi(char1.c_str()));

				}
				matrix.push_back(linha);
			}
			mapa.push_back(matrix);
		}
		myfile.close();
		
	}

	else cout << "Unable to open file";

	return mapa;
	
}