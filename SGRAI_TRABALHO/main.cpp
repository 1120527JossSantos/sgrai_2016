#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>
#include <vector>
#include <iostream>
#include "CarregaMapa.h"
#include "main.h"
#include<Windows.h>
#include <list>
#include <algorithm>
#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"
#include <string>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define DEBUG               1

#define DELAY_MOVIMENTO     20
#define RAIO_ROTACAO        20

/* VARIAVEIS GLOBAIS */

typedef struct {
	GLboolean   q, a, z, x, up, down, left, right;
}Teclas;

typedef struct {
	GLfloat    x, y, z;
}Pos;

typedef struct {
	Pos      eye, center, up;
	GLfloat  fov;
}Camera;

GLboolean nearBo=GL_FALSE;

typedef struct {
	GLboolean   doubleBuffer = GL_TRUE;
	GLint       delayMovimento;
	Teclas      teclas;
	GLuint      menu_id;
	GLboolean   menuActivo;
	Camera      camera;
	GLboolean   debug;
	GLboolean   ortho;
	GLboolean	stDraw=GL_TRUE;
	GLint		pontuacao;
}Estado;

typedef struct Coordenadas{
	GLint coorX, coorY, coorZ;
}Coordenadas;

typedef struct CoordenadasMoeda {
	GLint coorX, coorY, coorZ, rotate, rotX, rotY, rotZ, angX, angY, angZ, desenha;
}CoordenadasMoeda;

typedef struct {
	GLfloat x, y, z;
	GLfloat velocidadeX;
	GLfloat velocidadeY;
	GLint direccao;//0 � para dentro do ecran, 1 para fora, 2 para a esquerda, 3 para direita, 4 para cima e 5 para baixo
	GLint contacto;//0 � em baixo, 1 � em cima, 2 � esquerda , 3 � direita, 4 � frente e 5 � tras
	Coordenadas coor;
	GLfloat anguloX,anguloY,anguloZ;
}Esfera;

typedef struct {
	Esfera esfera;
	GLboolean   parado;
	GLboolean started=GL_TRUE;
	GLuint textura[2];
	StudioModel coin;
}Modelo;

typedef struct {
	int         sizeX, sizeY, bpp;
	char        *data;
}JPGImage;

typedef struct {
	GLuint texturaMenu[1];
}Menu;

typedef struct {
	GLuint texturaHelp[1];
}Help;

typedef struct Mapa
{
	std::vector<std::vector<std::vector<int>>> mapa;
}Mapa;
//typedef struct Funcoes
//{
//	std::vector<functions> funcoes;
//}Funcoes;

Joss joss;
Estado estado;
Modelo modelo;
Mapa mapa;
Menu menu;
Help help;

typedef struct Blocos {
	std::list<Coordenadas> blocos;
}Blocos;

typedef struct Moedas
{
	std::list<CoordenadasMoeda> moedas;
}Moedas;

GLint rot = 0;
Blocos blocos;
Moedas moedas;

extern "C" int read_JPEG_file(char *, char **, int *, int *, int *);

//Iluminacao
void setLight()
{
	GLfloat light_pos[4] = { -5.0, 5.0, 5.0, 0.0 };
	GLfloat light_pos2[4] = { 5.0, -5.0, 5.0, 0.0 };
	GLfloat light_pos3[4] = { 5.0, 5.0, -5.0, 0.0 };
	GLfloat light_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat light_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat light_specular[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	// laar ilumina��o
	glEnable(GL_LIGHTING);
	// ligar e definir fonte de light 0
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	// ligar e definir fonte de light 1
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light_pos2);

	// ligar e definir fonte de light 2
	/*glEnable(GL_LIGHT2);
	glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT2, GL_POSITION, light_pos3);*/

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
}

//Material
void setMaterial()
{
	GLfloat mat_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mat_shininess = 104;
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	// definir os outros par�metros estaticamente
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

void inicia_modelo()
{
	modelo.esfera.direccao = 0;
	modelo.esfera.velocidadeX = 0;
	modelo.esfera.velocidadeY = 0;
	modelo.esfera.x = 0;
	modelo.esfera.y = 0;
	modelo.esfera.direccao = 0;
	modelo.esfera.anguloX = 0;
	modelo.esfera.anguloY = 0;
	modelo.esfera.anguloZ = 0;


}

/* Texturas*/
void gerarTexturas()
{
	JPGImage imagemJPG;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glGenTextures(4, modelo.textura);

	(void)read_JPEG_file("basket.jpg", &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp);
	glBindTexture(GL_TEXTURE_2D, modelo.textura[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);

	(void)read_JPEG_file("grass.jpg", &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp);
	glBindTexture(GL_TEXTURE_2D, modelo.textura[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);

	(void)read_JPEG_file("tennis.jpg", &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp);
	glBindTexture(GL_TEXTURE_2D, modelo.textura[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);

	(void)read_JPEG_file("sky.jpg", &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp);
	glBindTexture(GL_TEXTURE_2D, modelo.textura[3]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

/* Menu Texturas*/
void gerarTexturasMenu()
{
	JPGImage imagemJPG;
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glGenTextures(1, menu.texturaMenu);

	(void)read_JPEG_file("menuPrincipal.jpg", &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp);
	glBindTexture(GL_TEXTURE_2D, menu.texturaMenu[0]);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

/* Help Texturas*/
void gerarTexturasHelp()
{
	JPGImage imagemJPG;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glGenTextures(1, help.texturaHelp);

	(void)read_JPEG_file("help.jpg", &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp);
	glBindTexture(GL_TEXTURE_2D, help.texturaHelp[0]);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}


/* Inicializa��o do ambiente OPENGL */
void Init(void)
{
	GLfloat amb[] = { 0.3f, 0.3f, 0.3f, 1.0f };
	srand((unsigned)time(NULL));
	gerarTexturasMenu();
	gerarTexturasHelp();
	mapa.mapa=joss.carregaDeFicheiro();
	modelo.parado = GL_FALSE;

	estado.debug = DEBUG;
	estado.menuActivo = GL_FALSE;
	estado.delayMovimento = DELAY_MOVIMENTO;
	estado.camera.up.x = 0;
	estado.camera.up.y = 1;
	estado.camera.up.z = 0;
	estado.ortho=GL_TRUE;
	estado.camera.fov = 90;
	estado.pontuacao = 0;

	estado.teclas.a = estado.teclas.q = estado.teclas.z = estado.teclas.x = \
		estado.teclas.up = estado.teclas.down = estado.teclas.left = estado.teclas.right = GL_FALSE;

	inicia_modelo();
	
	gerarTexturas();
	
	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	//glutIgnoreKeyRepeat(GL_TRUE);
	PlaySound("sound.wav", NULL, SND_ASYNC | SND_FILENAME | SND_LOOP);
	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);
}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint)width, (GLint)height);


	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	setLight();
	setMaterial();
	// gluOrtho2D(left,right,bottom,top); 
	// projeccao ortogonal 2D, com profundidade (Z) entre -1 e 1
	if (estado.debug)
		printf("Reshape %s\n", (estado.ortho) ? "ORTHO" : "PERSPECTIVE");

	if (estado.stDraw) {
		estado.camera.fov = 60;
		gluPerspective(estado.camera.fov, (GLfloat)width / height, 3, 60);
		
	}
	else {
		estado.camera.fov = 90;
		gluPerspective(estado.camera.fov, (GLfloat)width / height, 3.5, 60);
	}

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}


void desenhamoeda(int i) {
	glPushMatrix();
	glTranslatef(0, 0, 0);


	if (i == 0) {
		glRotatef(-90, 1, 0, 0);
		glRotatef(rot, 0, 0, 1);
	}
	else {
		glTranslatef(1, 0, 0);
		glRotatef(-90, 0, 1, 0);
		glRotatef(rot, 0, 0, 1);


	}
	glScalef(0.10, 0.10, 0.10);
	mdlviewer_display(modelo.coin);
	glPopMatrix();
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat cor[])
{

	glBegin(GL_POLYGON);
	glColor3fv(cor);
	glTexCoord2f(0.0, 0.0);
	glVertex3fv(a);
	glTexCoord2f(1.0, 0.0);
	glVertex3fv(b);
	glTexCoord2f(1.0, 1.0);
	glVertex3fv(c);
	glTexCoord2f(0.0, 1.0);
	glVertex3fv(d);
	glEnd();
}


void desenhaCubo()
{
	GLfloat vertices[][3] = { { -1.7,-1.7,-1.7 },
	{ 1.7,-1.7,-1.7 },
	{ 1.7,1.7,-1.7 },
	{ -1.7,1.7,-1.7 },
	{ -1.7,-1.7,1.7 },
	{ 1.7,-1.7,1.7 },
	{ 1.7,1.7,1.7 },
	{ -1.7,1.7,1.7 } };

	GLfloat cores[][3] = { { 1.0,1.0,1.0 },
	{ 1.0,1.0,1.0 },
	{ 1.0,1.0,1.0 },
	{ 1.0,1.0,1.0 },
	{ 1.0,1.0,1.0 },
	{ 1.0,1.0,1.0 },
	{ 1.0,1.0,1.0 } };

	glBindTexture(GL_TEXTURE_2D, modelo.textura[1]);
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], cores[0]);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], cores[1]);
	glBindTexture(GL_TEXTURE_2D, modelo.textura[1]);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], cores[2]);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], cores[3]);
	glBindTexture(GL_TEXTURE_2D, modelo.textura[1]);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], cores[4]);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], cores[5]);
}


void strokeString(char *str, double x, double y, double z, double s)
{
	int i, n;

	n = strlen(str);
	glPushMatrix();
	glColor3d(0.0, 0.0, 0.0);
	glTranslated(x, y, z);
	glScaled(s, s, s);
	for (i = 0; i<n; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)str[i]);

	glPopMatrix();

}

void bitmapString(char *str, double x, double y)
{
	int i, n;

	// fonte pode ser:
	// GLUT_BITMAP_8_BY_13
	// GLUT_BITMAP_9_BY_15
	// GLUT_BITMAP_TIMES_ROMAN_10
	// GLUT_BITMAP_TIMES_ROMAN_24
	// GLUT_BITMAP_HELVETICA_10
	// GLUT_BITMAP_HELVETICA_12
	// GLUT_BITMAP_HELVETICA_18
	//
	// int glutBitmapWidth  	(	void *font , int character);
	// devolve a largura de um car�cter
	//
	// int glutBitmapLength 	(	void *font , const unsigned char *string );
	// devolve a largura de uma string (soma da largura de todos os caracteres)

	n = strlen(str);
	glRasterPos2d(x, y);
	for (i = 0; i<n; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}

void bitmapCenterString(char *str, double x, double y)
{
	int i, n;

	n = strlen(str);
	glRasterPos2d(x - glutBitmapLength(GLUT_BITMAP_HELVETICA_18, (const unsigned char *)str)*0.5, y);
	for (i = 0; i<n; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}

void strokeCenterString(char *str, double x, double y, double z, double s)
{
	int i, n;

	n = strlen(str);
	glPushMatrix();
	glTranslated(x - glutStrokeLength(GLUT_STROKE_ROMAN, (const unsigned char*)str)*0.5*s, y - 119.05*0.5*s, z);
	glScaled(s, s, s);
	for (i = 0; i<n; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)str[i]);
	glPopMatrix();

}


// ... definicao das rotinas auxiliares de desenho ...




// Callback de desenho
void desenhaMapa() {
	glPushMatrix();
	glColor3f(0.5f, 0.5f, 0.5f);

	
	for (int i = 0;i < mapa.mapa.size();i++) {//ciclo para o andar
		glPushMatrix();
		for (int j = 0;j < mapa.mapa[i].size();j++) {
			glPushMatrix();
			for (int k = 0;k < mapa.mapa[i][j].size();k++) {
				if (mapa.mapa[i][j][k] == 1) {
					desenhaCubo();
					if (estado.stDraw) {
						Coordenadas coor = { k,i,j };
						blocos.blocos.push_back(coor);
					}
				}
				if (mapa.mapa[i][j][k] == 2) {
					if (modelo.started) {
						modelo.esfera.x = k * 3.4;
						modelo.esfera.coor.coorX = k;
						modelo.esfera.y = i * 3.4;
						modelo.esfera.coor.coorY = i;
						modelo.esfera.z = j * 3.4;
						modelo.esfera.coor.coorZ = j;
						glPushMatrix();
						estado.camera.eye.x = modelo.esfera.x + 7;
						estado.camera.eye.y = modelo.esfera.y + 7;
						estado.camera.eye.z = modelo.esfera.z;
						estado.camera.center.x = modelo.esfera.x;
						estado.camera.center.y = modelo.esfera.y;
						estado.camera.center.z = modelo.esfera.z;
						glPopMatrix();

					}
					modelo.started = GL_FALSE;
				}
				if (mapa.mapa[i][j][k] == 3) {
					if (estado.stDraw) {
						CoordenadasMoeda coor = { k,i,j,0,0,0,0,0,0,0,1 };
						moedas.moedas.push_back(coor);
					}
				}
				if (mapa.mapa[i][j][k] == 4) {
					if (estado.stDraw) {
						CoordenadasMoeda coor = { k,i,j,-90,0,0,1,0,0,0,1 };
						moedas.moedas.push_back(coor);
					}
				}
				glTranslatef(3.4, 0.0, 0.0);
			}
			glPopMatrix();
			glTranslatef(0.0, 0.0, 3.4);
		}
		glPopMatrix();
		glTranslatef(0.0, 3.4, 0.0);
	}
	glPopMatrix();
	estado.stDraw = GL_FALSE;
}

void desenhaMoedas() {
	for (std::list<CoordenadasMoeda>::iterator it = moedas.moedas.begin(); it != moedas.moedas.end(); ++it) {
		if (it->desenha) {
			glPushMatrix();
			glTranslatef(it->coorX*3.4, it->coorY*3.4, it->coorZ*3.4);
			int i = 1;
			if (it->rotate == 0)
				i = 0;
			desenhamoeda(i);
			glPopMatrix();
		}
	}
}
void verificaApanhouMoeda() {
	for (std::list<CoordenadasMoeda>::iterator it = moedas.moedas.begin(); it != moedas.moedas.end(); ++it) {
		if (modelo.esfera.coor.coorX == it->coorX && modelo.esfera.coor.coorY == it->coorY && modelo.esfera.coor.coorZ == it->coorZ) {
			if (it->desenha == 1) {
				estado.pontuacao += 5;
				/*PlaySound("coingained.wav", NULL, SND_ASYNC | SND_FILENAME );
				PlaySound("music.wav", NULL, SND_ASYNC | SND_FILENAME);*/
			}
			it->desenha = 0;
			
			
		}
	}
}

GLboolean inMotion;
GLint increment;
GLfloat modeloX, modeloY, modeloZ, camaraX,  camaraY, camaraZ;
void desenhaPontuacao(int width, int height)
{

	// Altera viewport e projec��o

	glViewport(width - 90, 0, 70, 90);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluOrtho2D(-30, 30, -30, 30);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	//glRotatef(GRAUS(estado.camera.dir_long + estado.camera.dir_lat) - 90, 0, 0, 1);

	/*glBegin(GL_TRIANGLES);
	glColor4f(0, 0, 0, 0.2);
	glVertex2f(0, 15);
	glVertex2f(-6, 0);
	glVertex2f(6, 0);
	glColor4f(1, 1, 1, 0.2);
	glVertex2f(6, 0);
	glVertex2f(-6, 0);
	glVertex2f(0, -15);
	glEnd();*/

	glLineWidth(5.0);
	glColor3f(1.0, 1.0, 1.0);
	std::string s = std::to_string(estado.pontuacao);
	char* pchar = (char*) s.c_str();
	strokeCenterString(pchar, 5, 0, 0, 0.3);
	//strokeCenterString("", 5, 0, 0, 0.2);
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);


	//rep�e projec��o chamando redisplay
	//reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
}
void mexerCamara() {
	if(increment<15){
		modelo.esfera.x += modeloX / 15;
		modelo.esfera.y += modeloY / 15;
		modelo.esfera.z += modeloZ / 15;
		estado.camera.eye.x += camaraX / 15;
		estado.camera.eye.y += camaraY / 15;
		estado.camera.eye.z += camaraZ / 15;
		estado.camera.center.x = modelo.esfera.x; 
		estado.camera.center.y = modelo.esfera.y; 
		estado.camera.center.z = modelo.esfera.z;
		if (modeloX < 0) {
			modelo.esfera.anguloZ += 4;
		}
		if (modeloX > 0) {
			modelo.esfera.anguloZ -= 4;
		}
		if (modeloZ < 0) {
			modelo.esfera.anguloX -= 4;
		}
		if (modeloZ > 0) {
			modelo.esfera.anguloX += 4;
		}
		if (modeloY > 0) {
			modelo.esfera.anguloZ += 4;
		}
		if (modeloY < 0) {
			modelo.esfera.anguloZ -= 4;
		}
		increment++;
	}
	else {
		inMotion = GL_FALSE;
		increment = 0;
		modeloX = 0;
		modeloY = 0;
		modeloZ = 0;
		camaraX = 0;
		camaraY = 0;
		camaraZ = 0;
	}
}

void Draw(void)
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	if (inMotion) {
		mexerCamara();
	}
	gluLookAt(estado.camera.eye.x, estado.camera.eye.y, estado.camera.eye.z,
		estado.camera.center.x, estado.camera.center.y, estado.camera.center.z,
		estado.camera.up.x, estado.camera.up.y, estado.camera.up.z);
	glDisable(GL_DEPTH_TEST);
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, modelo.textura[3]);
	GLUquadricObj* l_poQuadric2 = gluNewQuadric();
	glTranslatef(modelo.esfera.x,modelo.esfera.y,modelo.esfera.z);
	glRotatef(90, 1, 0, 0);
	
	gluQuadricOrientation(l_poQuadric2, GLU_INSIDE);
	gluQuadricDrawStyle(l_poQuadric2, GLU_FILL);
	gluQuadricNormals(l_poQuadric2, GLU_SMOOTH);
	gluQuadricTexture(l_poQuadric2, GL_TRUE);
	gluSphere(l_poQuadric2, 50, 20, 50);
	gluDeleteQuadric(l_poQuadric2);
	//desenhaEsfera(modelo.esfera);
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
	desenhaMapa();
	/*std::cout << "mylist contains:\n";
	for (std::list<Coordenadas>::iterator it = blocos.blocos.begin(); it != blocos.blocos.end(); ++it)
		std::cout << ' ' << it->coorX<< " x - "<< it->coorY<<" y - "<< it->coorZ<<" z .";
	*/
	
	glColor3f(0.5f, 0.5f, 0.5f);
	//desenhaChao(RAIO_ROTACAO - 5);
	


	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	glTranslatef(modelo.esfera.x, modelo.esfera.y, modelo.esfera.z);
	glRotatef(modelo.esfera.anguloZ, 0, 0, 1);
	glRotatef(modelo.esfera.anguloY, 0, 1, 0);
	glRotatef(modelo.esfera.anguloX, 1, 0, 0);
	glBindTexture(GL_TEXTURE_2D, modelo.textura[0]);
	GLUquadricObj* l_poQuadric = gluNewQuadric();
	gluQuadricDrawStyle(l_poQuadric, GLU_FILL);
	gluQuadricNormals(l_poQuadric, GLU_SMOOTH);
	gluQuadricTexture(l_poQuadric, GL_TRUE);
	gluSphere(l_poQuadric, 1, 20, 50);
	gluDeleteQuadric(l_poQuadric);
	//desenhaEsfera(modelo.esfera);
	glPopMatrix();

	glPushMatrix();
	desenhaMoedas();
	glPopMatrix();
	rot++;
	rot++;
	verificaApanhouMoeda();

	desenhaPontuacao(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();

	

}

void drawMenu() {
	glEnable(GL_TEXTURE_2D);
	//Clear information from last draw
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective
	glBindTexture(GL_TEXTURE_2D, menu.texturaMenu[0]);
	glBegin(GL_QUADS); //Begin quadrilateral coordinates
					   //Trapezoid
	glTexCoord2d(0.0, 1.0);
	glVertex3f(-7.0f, -3.5f, -6.0f);
	glTexCoord2d(0.0, 0.0);
	glVertex3f(-7.0f, 3.5f, -6.0f);
	glTexCoord2d(1.0, 0.0);
	glVertex3f(7.0f, 3.5f, -6.0f);
	glTexCoord2d(1.0, 1.0);
	glVertex3f(7.0f, -3.5f, -6.0f);
	glEnd(); //End quadrilateral coordinatesf
	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();

}

void drawHelp() {
	glEnable(GL_TEXTURE_2D);
	//Clear information from last draw
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective
	glLoadIdentity(); //Reset the drawing perspective
	glBindTexture(GL_TEXTURE_2D, help.texturaHelp[0]);
	glBegin(GL_QUADS); //Begin quadrilateral coordinates
					   //Trapezoid
	glTexCoord2d(0.0, 1.0);
	glVertex3f(-7.0f, -3.5f, -6.0f);
	glTexCoord2d(0.0, 0.0);
	glVertex3f(-7.0f, 3.5f, -6.0f);
	glTexCoord2d(1.0, 0.0);
	glVertex3f(7.0f, 3.5f, -6.0f);
	glTexCoord2d(1.0, 1.0);
	glVertex3f(7.0f, -3.5f, -6.0f);
	glEnd(); //End quadrilateral coordinatesf
	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();

}
void virarPOVDireita() {
	if (modelo.esfera.contacto == 0) {
		switch (modelo.esfera.direccao) {
		case 0:
			inMotion = GLU_TRUE;
			camaraZ = -7;
			camaraX = -7;
			modelo.esfera.direccao = 2;
			break;
		case 2:

			inMotion = GLU_TRUE;
			camaraX = -7;
			camaraZ = +7;
			modelo.esfera.direccao = 1;
			break;
		case 1:
			inMotion = GLU_TRUE;
			camaraZ = +7;
			camaraX = +7;
			modelo.esfera.direccao = 3;
			break;
		case 3:
			inMotion = GLU_TRUE;
			camaraX = +7;
			camaraZ = -7;
			modelo.esfera.direccao = 0;
			break;
		}
	}
	if (modelo.esfera.contacto == 4)
	{
		switch (modelo.esfera.direccao) {
		case 4:
			inMotion = GLU_TRUE;
			camaraZ -= 7;
			camaraY += 2;
			modelo.esfera.direccao = 2;
			break;
		case 2:
			inMotion = GLU_TRUE;
			camaraZ += 7;
			camaraY += 7;
			modelo.esfera.direccao = 5;
			break;
		case 5:
			inMotion = GLU_TRUE;
			camaraZ += 7;
			camaraY -= 7;
			modelo.esfera.direccao = 3;
			break;
		case 3:
			inMotion = GLU_TRUE;
			camaraZ -= 7;
			if (nearBo) { camaraY -= 2; 
			}else {
				camaraY -= 7;
			}
			modelo.esfera.direccao = 4;

		}
	}
}

void virarPOVEsquerda() {
	if (modelo.esfera.contacto == 0) {
		switch (modelo.esfera.direccao) {
		case 2:
			inMotion = GLU_TRUE;
			camaraZ = +7;
			camaraX = +7;
			modelo.esfera.direccao = 0;
			break;
		case 1:
			inMotion = GLU_TRUE;
			camaraZ = -7;
			camaraX = +7;
			modelo.esfera.direccao = 2;
			break;
		case 3:
			inMotion = GLU_TRUE;
			camaraZ = -7;
			camaraX = -7;
			modelo.esfera.direccao = 1;
			break;
		case 0:
			inMotion = GLU_TRUE;
			camaraZ = +7;
			camaraX = -7;
			modelo.esfera.direccao = 3;
			break;
		}
	}
	if (modelo.esfera.contacto == 4)
	{
		estado.camera;
		switch (modelo.esfera.direccao) {
		case 4:
			inMotion = GLU_TRUE;
			camaraZ += 7;
			camaraY += 2;
			modelo.esfera.direccao = 3;
			break;
		case 2:
			inMotion = GLU_TRUE;
			camaraZ += 7;
			camaraY -= 2;
			modelo.esfera.direccao = 4;
			break;
		case 5:
			inMotion = GLU_TRUE;
			camaraZ -= 7;
			camaraY -= 7;
			modelo.esfera.direccao = 2;
			break;
		case 3:
			inMotion = GLU_TRUE;
			camaraZ -= 7;
			camaraY += 7;
			modelo.esfera.direccao = 5;
			break;
		}
	}
}
GLboolean VerificaBlocos(Coordenadas coor) {
	for (std::list<Coordenadas>::iterator it = blocos.blocos.begin(); it != blocos.blocos.end(); ++it) {
		if (it->coorX == coor.coorX && it->coorY == coor.coorY && it->coorZ == coor.coorZ)
			return true;
	}
		
	return false;
}

/*******************************
***   callbacks timer/idle   ***
*******************************/
void andarEmFrente() {
	
	if (modelo.esfera.contacto == 0) {
		if (modelo.esfera.direccao == 0) {
			Coordenadas subir = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY ,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(subir)) {
				estado.camera.eye.y = modelo.esfera.y;
				estado.camera.up.y = 0;
				estado.camera.up.x = 1;
				estado.camera.up.z = 0;
				inMotion = GLU_TRUE;
				camaraY = -2;
				modelo.esfera.direccao = 4;
				modelo.esfera.contacto = 4;
				nearBo = GL_TRUE;
			}
			else {
				Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ };
				if (VerificaBlocos(coor)) {
					inMotion = GLU_TRUE;
					modeloX = -3.4;
					camaraX = -3.4;
					modelo.esfera.coor.coorX -= 1;
					modelo.esfera.anguloZ += 8;
					nearBo = GL_FALSE;
				}
			}
		}
		if (modelo.esfera.direccao == 2) {
			Coordenadas coor = { modelo.esfera.coor.coorX ,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ + 1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = +3.4;
				camaraZ = +3.4;
				modelo.esfera.coor.coorZ += 1;
				modelo.esfera.anguloX -= 8;
				nearBo = GL_FALSE;
			}
		}
		if (modelo.esfera.direccao == 1) {
			Coordenadas coor = { modelo.esfera.coor.coorX + 1,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloX = +3.4;
				camaraX = +3.4;
				modelo.esfera.coor.coorX += 1;
				modelo.esfera.anguloZ -= 8;
				nearBo = GL_FALSE;
			}
		}
		if (modelo.esfera.direccao == 3) {
			Coordenadas coor = { modelo.esfera.coor.coorX ,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ - 1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = -3.4;
				camaraZ = -3.4;
				modelo.esfera.coor.coorZ -= 1;
				modelo.esfera.anguloX += 8;
				nearBo = GL_FALSE;
			}
		}
		return;
	}
	
	if (modelo.esfera.contacto == 4) {
		if (modelo.esfera.direccao == 4) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY + 1,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloY = +3.4;
				camaraY = +3.4;
				modelo.esfera.coor.coorY += 1;
				modelo.esfera.anguloZ += 8;
			}
		}
		if (modelo.esfera.direccao == 5) {
			Coordenadas subir = { modelo.esfera.coor.coorX + 1,modelo.esfera.coor.coorY -1,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(subir)) {
				estado.camera.up.y = 1;
				estado.camera.up.x = 0;
				estado.camera.up.z = 0;
				estado.camera.eye.x = modelo.esfera.x - 7;
				modelo.esfera.direccao = 1;
				modelo.esfera.contacto = 0;
			}
			else {
				Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ };
				if (VerificaBlocos(coor)) {
					inMotion = GLU_TRUE;
					modeloY = -3.4;
					camaraY = -3.4;
					modelo.esfera.coor.coorY -= 1;
					modelo.esfera.anguloZ -= 8;
				}
			}
		}
		if (modelo.esfera.direccao == 3) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY ,modelo.esfera.coor.coorZ-1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = -3.4;
				camaraZ = -3.4;
				modelo.esfera.coor.coorZ -= 1;
				
				modelo.esfera.anguloY -= 8;
			}
		}
		if (modelo.esfera.direccao == 2) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY ,modelo.esfera.coor.coorZ + 1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = +3.4;
				camaraZ = +3.4;
				modelo.esfera.coor.coorZ += 1;

				modelo.esfera.anguloY += 8;
			}
		}
		return;
	}

}
void virarCamaraDireita() {
	switch (modelo.esfera.contacto)
	{
	case 0:
		virarPOVDireita();
		break;
	default:
		virarPOVDireita();
		break;
	}
}
void virarCamaraEsquerda() {
	switch (modelo.esfera.contacto)
	{
	case 0:
		virarPOVEsquerda();
		break;
	default:
		virarPOVEsquerda();
		break;
	}

}

void andarParaTras() {
	if (modelo.esfera.contacto == 0) {
		if (modelo.esfera.direccao == 0) {
			Coordenadas coor = { modelo.esfera.coor.coorX + 1,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloX = +3.4;
				camaraX = +3.4;
				modelo.esfera.coor.coorX += 1;

			}
		}
		if (modelo.esfera.direccao == 2) {
			Coordenadas coor = { modelo.esfera.coor.coorX,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ - 1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = -3.4;
				camaraZ = -3.4;
				modelo.esfera.coor.coorZ -= 1;
			
			}
		}
		if (modelo.esfera.direccao == 1) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloX = -3.4;
				camaraX = -3.4;
				modelo.esfera.coor.coorX -= 1;
			}
		}
		if (modelo.esfera.direccao == 3) {
			Coordenadas coor = { modelo.esfera.coor.coorX ,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ + 1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = +3.4;
				camaraZ = +3.4;
				modelo.esfera.coor.coorZ += 1;
			}
		}
		return;
	}
	if (modelo.esfera.contacto == 4) {
		if (modelo.esfera.direccao == 4) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY - 1,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloY = -3.4;
				camaraY = -3.4;
				modelo.esfera.coor.coorY -= 1;
			}
		}
		if (modelo.esfera.direccao == 5) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY + 1,modelo.esfera.coor.coorZ };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloY = +3.4;
				camaraY = +3.4;
				modelo.esfera.coor.coorY += 1;

			}
		}
		if (modelo.esfera.direccao == 3) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY ,modelo.esfera.coor.coorZ + 1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = +3.4;
				camaraZ = +3.4;
				modelo.esfera.coor.coorZ += 1;

			}
		}
		if (modelo.esfera.direccao == 2) {
			Coordenadas coor = { modelo.esfera.coor.coorX - 1,modelo.esfera.coor.coorY ,modelo.esfera.coor.coorZ - 1 };
			if (VerificaBlocos(coor)) {
				inMotion = GLU_TRUE;
				modeloZ = -3.4;
				camaraZ = -3.4;
				modelo.esfera.coor.coorZ -= 1;
				estado.camera.center.z = modelo.esfera.z;
				modelo.esfera.anguloY -= 8;
			}
		}
		return;
	}
}
void Timer(int value)
{
	glutTimerFunc(estado.delayMovimento, Timer, 0);
	/* ... accoes do temporizador ... */

	if (estado.teclas.up) { //&& modelo.esfera.velocidadeX<0.4)
		andarEmFrente();
		estado.pontuacao++;
	}
		
	if (estado.teclas.down) //&& modelo.esfera.velocidadeX>-0.4)
		andarParaTras();
		
	if (estado.teclas.left) //&& modelo.esfera.velocidadeY < 0.4)
		virarCamaraEsquerda();
		
	if (estado.teclas.right) //&& modelo.esfera.velocidadeY > -0.4)
		virarCamaraDireita();
		



	if (estado.menuActivo || modelo.parado) // sair em caso de o jogo estar parado ou menu estar activo
		return;

	
	glutPostRedisplay();
}



void imprime_ajuda(void)
{
	printf("\n\nJogo\n");
	printf("h,H - Ajuda \n");
	printf("ESC - Sair\n");
}

/*******************************
***  callbacks de rato    ***
*******************************/

void processMouseMenu(int button, int state, int x, int y) {
	//bot�o esquerdo premido
	if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON) {
		//New Game
		if (y > 154.0f && y < 230.0f && x> 254.0f && x<754.0f) {
			glutDisplayFunc(Draw);
			//Retira hip�tese de usar a callback de rato para n�o dar erro durante o jogo
			glutMouseFunc(NULL);
		}
		//Help
		else if (y>260.0f && y< 330.0f && x>400.0f && x<610.0f) {
			glutDisplayFunc(drawHelp);
			//Retira hip�tese de usar a callback de rato para n�o se come�ar jogo a partir do help
			glutMouseFunc(NULL);
		}
		//Quit
		else if (y>368.0f && y<425.0f && x>415.0f && x<600.f) {
			exit(1);
		}
	}
}
/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interaccao via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)
{
	switch (key) {

	case 27:
		exit(1);
		// ... accoes sobre outras teclas ... 

	case 'h':
	case 'H':
		imprime_ajuda();
		break;
	case 'o':
	case 'O':
		break;
	case 'Q':
	case 'q': estado.teclas.q = GL_TRUE;
		break;
	case 'A':
	case 'a': estado.teclas.a = GL_TRUE;
		break;
	case 'Z':
	case 'z': estado.teclas.z = GL_TRUE;
		break;
	case 'X':
	case 'x': estado.teclas.x = GL_TRUE;
		break;
	case 'D':
	case 'd': estado.debug = !estado.debug;
		break;
	case 'f':
	case 'F':
		break;
	case 'p':
	case 'P':glutDisplayFunc(drawMenu);
		break;
	case 'l':
	case 'L':
		break;
	case 8: //Backspace usado no Help
		glutDisplayFunc(drawMenu);
		//Rep�e callback de rato quando volta ao menu
		glutMouseFunc(processMouseMenu);
		break;
	}

	if (estado.debug)
		printf("Carregou na tecla %c\n", key);

}

// Callback para interaccao via teclado (largar a tecla)

void KeyUp(unsigned char key, int x, int y)
{
	switch (key) {
		// ... accoes sobre largar teclas ... 

	}

	if (estado.debug)
		printf("Largou a tecla %c\n", key);
}

//// Callback para interaccao via teclas especiais  (carregar na teclaif (estado.teclas.up) //&& modelo.esfera.velocidadeX<0.4)
//		modelo.esfera.y -= 0.6;
//		if (estado.teclas.down) //&& modelo.esfera.velocidadeX>-0.4)
//			modelo.esfera.y += 0.6;
//		if (estado.teclas.left) //&& modelo.esfera.velocidadeY < 0.4)
//			modelo.esfera.x += 0.6;
//		if (estado.teclas.right) //&& modelo.esfera.velocidadeY > -0.4)
//			modelo.esfera.x -= 0.6;)

void SpecialKey(int key, int x, int y)
{
	// ... accoes sobre outras teclas especiais ... 
	//    GLUT_KEY_F1 ... GLUT_KEY_F12
	//    GLUT_KEY_UP
	//    GLUT_KEY_DOWN
	//    GLUT_KEY_LEFT
	//    GLUT_KEY_RIGHT
	//    GLUT_KEY_PAGE_UP
	//    GLUT_KEY_PAGE_DOWN
	//    GLUT_KEY_HOME
	//    GLUT_KEY_END
	//    GLUT_KEY_INSERT 

	switch (key) {
	case GLUT_KEY_RIGHT:
		estado.teclas.right = GL_TRUE;
		
		estado.teclas.right = GL_FALSE;
		break;
	case GLUT_KEY_LEFT:
		estado.teclas.left = GL_TRUE;
	
		estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_UP:
		estado.teclas.up = GL_TRUE;
		
		estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_TRUE;
		
		estado.teclas.down = GL_FALSE;
		break;


		// redesenhar o ecra 
		//glutPostRedisplay();
	}


	if (estado.debug)
		printf("Carregou na tecla especial %d\n", key);
}

// Callback para interaccao via teclas especiais (largar na tecla)

void SpecialKeyUp(int key, int x, int y)
{
	if (inMotion) {

	}
	else {
		switch (key) {
		case GLUT_KEY_RIGHT:
			virarCamaraDireita();
			estado.teclas.right = GL_FALSE;
			break;
		case GLUT_KEY_LEFT:
			virarCamaraEsquerda();
			estado.teclas.left = GL_FALSE;
			break;
		case GLUT_KEY_UP:
			andarEmFrente();
			estado.teclas.up = GL_FALSE;
			break;
		case GLUT_KEY_DOWN:
			andarParaTras();
			estado.teclas.down = GL_FALSE;
			break;

		}
		if (estado.debug)
			printf("Largou a tecla especial %d\n", key);
	}
}





int main(int argc, char **argv)
{
	char str[] = " makefile MAKEFILE Makefile ";
	estado.doubleBuffer = 1;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1024, 512);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Kula World") == GL_FALSE)
		exit(1);

	Init();
	setLight();
	setMaterial();
	mdlviewer_init("mario_coin.mdl", modelo.coin);
	imprime_ajuda();

	// Registar callbacks do GLUT

	// callbacks de janelas/desenho
	glutReshapeFunc(Reshape);
	glutDisplayFunc(drawMenu);
	//glutDisplayFunc(Draw);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// Callback de rato
	glutMouseFunc(processMouseMenu);

	// callbacks timer/idle
	glutTimerFunc(estado.delayMovimento, Timer, 0);

	// COMECAR...
	glutMainLoop();
	return 0;
}

